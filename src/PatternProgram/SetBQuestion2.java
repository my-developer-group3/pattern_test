package PatternProgram;

public class SetBQuestion2 {
    public static void main(String[] args) {
        int line=5;                                 //A B C D E
        char ch='A';                                //F G A B C
        for (int i=0;i<line;i++)                    //D E F G A
        {                                           //B C D E F
            for (int j=0; j<5; j++)                 //G A B C D
            {
                System.out.print(ch++ + "\t");
                if (ch >'G')
                {
                    ch='A';
                }
            }
            System.out.println();
        }
    }
}
