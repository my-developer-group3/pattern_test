package PatternProgram;

public class Question4 {
    public static void main(String[] args) {
        int line=9;
        int star=1;
        for(int i=0;i<line;i++)
        {
            for (int k=0;k<star;k++)
            {
                System.out.print("* ");
            }
            System.out.println();
            if(i<=3)
            {
                star++;
            }else
            {
                star--;
            }
        }
    }
}
